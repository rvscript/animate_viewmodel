package com.example.ga_mlsdiscovery.animationtest.view.list_fragment;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ga_mlsdiscovery.animationtest.R;
import com.example.ga_mlsdiscovery.animationtest.model.User;
import com.example.ga_mlsdiscovery.animationtest.view.interfaces.UserSelectedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {

    private final List<User> data = new ArrayList<>();
    private final UserSelectedListener userSelectedListener;
    //Include our ListViewModel and LifeCycleOwner
    //LifeCycleOwner will manage our ListViewModel data
    //userSelected Listener is for the onClick of the items
    //UserViewHolder will also need a userSelectedListener parameter
    public UserListAdapter(ListViewModel viewModel,
                           LifecycleOwner lifecycleOwner,
                           UserSelectedListener userSelectedListener) {
        this.userSelectedListener = userSelectedListener;
        //create a reference to the User's live data
        //pass in the Lifecycle owner and observe
        viewModel.getUsers().observe(lifecycleOwner, users -> {
            data.clear();
            if (users != null) {
                data.addAll(users);
                notifyDataSetChanged();//TODO: use DiffUtil when we have AutoValue models
            }

            //setHasStableIds(true);
        });
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new UserViewHolder(view, userSelectedListener );
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static final class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_user_name)
        TextView tv_un;
        @BindView(R.id.tv_user_description)
        TextView tv_desc;
        @BindView(R.id.tv_forks)
        TextView tv_forks;
        @BindView(R.id.tv_stars)
        TextView tv_stars;

        private User user;

        UserViewHolder(View itemView, UserSelectedListener userSelectedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(user != null){
                    userSelectedListener.onUserSelected(user);
                }
            });
        }

        //bind views in xml to pojo from models
        void bind(User user) {
            this.user = user;
            tv_un.setText(user.getUsername());
            tv_desc.setText(user.getName());
            tv_forks.setText(String.valueOf(user.getId()));
            tv_stars.setText(String.valueOf(user.getPhone()));
        }
    }
}
