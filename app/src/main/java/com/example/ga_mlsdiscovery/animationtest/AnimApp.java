package com.example.ga_mlsdiscovery.animationtest;

import android.app.Application;
import android.content.Context;

import com.example.ga_mlsdiscovery.animationtest.base.ApplicationComponent;
import com.example.ga_mlsdiscovery.animationtest.utils.NotLoggingTree;

import timber.log.Timber;

public class AnimApp extends Application {

    /*
    We can get reference to this component
    by adding dagger to the front of the component
    name
     */
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        } else
            Timber.plant(new NotLoggingTree());

        //Create an instance of the DaggerApplicationComponent
        //component = DaggerApplicationComponent.create();
    }

    //we need a way for other classes to get a reference to this
    //Any object may get a reference to this component using just the context
    public static ApplicationComponent getApplicationComponent(Context context){
        return((AnimApp)context.getApplicationContext()).component;
    }
}
