package com.example.ga_mlsdiscovery.animationtest.view.interfaces;

import com.example.ga_mlsdiscovery.animationtest.model.User;

public interface UserSelectedListener {
    void onUserSelected(User user);
}
