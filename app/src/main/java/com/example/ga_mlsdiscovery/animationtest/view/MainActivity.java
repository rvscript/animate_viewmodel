package com.example.ga_mlsdiscovery.animationtest.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ga_mlsdiscovery.animationtest.R;
import com.example.ga_mlsdiscovery.animationtest.view.list_fragment.UserListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.button1)
    Button button;
    @BindView(R.id.button2) Button mButton;
    @BindView(R.id.button3) Button button_3;
    @BindView(R.id.button4) Button button_4;
    @BindView(R.id.list_item)
    LinearLayout linearLayout;
    @BindView(R.id.list_item2) LinearLayout linearLayout2;
    @BindView(R.id.fragment_container) FrameLayout frameLayout;

    private FragmentManager fm;
    private boolean isItNull = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        button.setOnClickListener(this);
        mButton.setOnClickListener(this);
        button_3.setOnClickListener(this);
        button_4.setOnClickListener(this);
        linearLayout.setTranslationY(-2000f);
        if(savedInstanceState == null) {
            fm = getSupportFragmentManager();
//            fm
//                    .beginTransaction()
//                    .add(R.id.fragment_container, new UserListFragment())
//                    .addToBackStack(null)
//                    .commit();
        }
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.button1) {
            Toast.makeText(this, "Linear Layout should come down", Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
                linearLayout.animate()
                        .translationYBy(2000f)
                        .setInterpolator(new OvershootInterpolator(3f))
                        .setDuration(550);

                mButton.setRotation(0);
            mButton.animate()
                    .rotation(1800f)
                    .setInterpolator(new AnticipateOvershootInterpolator())
                    .setDuration(2000);

        } else if (v.getId() == R.id.button2) {
            mButton.setRotation(0);
            mButton.animate()
                    .rotation(1080f)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setDuration(2000);

            Toast.makeText(this, "Animation funky", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.button3) {

                Toast.makeText(this, "hello button3", Toast.LENGTH_SHORT).show();
                frameLayout.setVisibility(View.VISIBLE);

                fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        int i = fm.getBackStackEntryCount();
                        Log.d("onBackStack", "onBackStackChanged: = " + i);
                        if (i == 0) {
                            finish();
                            startActivity(getIntent());
                        }
                    }
                });
                fm
                        .beginTransaction()
                        .add(R.id.fragment_container, new FragmentAnimation())
                        .addToBackStack("fragment1")
                        .commit();

        } else if (v.getId() == R.id.button4){

            Toast.makeText(this, "To MVVM Fragment", Toast.LENGTH_SHORT).show();
            frameLayout.setVisibility(View.VISIBLE);

            fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    int i = fm.getBackStackEntryCount();
                    Timber.d("onBackStack", "onBackStackChanged: = "+i);
                    if(i == 0){
                        finish();
                        startActivity(getIntent());
                    }
                }
            });

                fm
                        .beginTransaction()
                        .replace(R.id.fragment_container, new UserListFragment())
                        .addToBackStack(null)
                        .commit();
        }


    }

}
