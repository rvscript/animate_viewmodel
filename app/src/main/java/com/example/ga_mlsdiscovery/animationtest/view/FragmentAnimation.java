package com.example.ga_mlsdiscovery.animationtest.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.ga_mlsdiscovery.animationtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentAnimation extends Fragment implements View.OnClickListener{

    @BindView(R.id.anim_button)Button button_admin;
    @BindView(R.id.layout)LinearLayout layout_1;
    private LinearLayout layout;
    private Unbinder unbinder;
    private boolean isTrue =true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.animation_fragment, container, false);

        unbinder = ButterKnife.bind(this, v);
        button_admin.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        if(isTrue) {
            layout_1.animate()
                    .scaleX(0.5f)
                    .scaleY(0.5f)
                    .setInterpolator(new BounceInterpolator())
                    .setDuration(2000);
            isTrue = false;
        } else {
            layout_1.animate()
                    .scaleX(1f)
                    .scaleY(1f)
                    .setDuration(2000);

            isTrue = true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder != null){
            unbinder.unbind();
        }
    }
}
