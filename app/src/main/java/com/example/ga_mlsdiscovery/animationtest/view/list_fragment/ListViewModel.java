package com.example.ga_mlsdiscovery.animationtest.view.list_fragment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.ga_mlsdiscovery.animationtest.model.User;
import com.example.ga_mlsdiscovery.animationtest.networking.UserApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ListViewModel extends ViewModel {

    private final MutableLiveData<List<User>> users = new MutableLiveData<>();
    private final MutableLiveData<Boolean> userLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    private Call<List<User>> userCall;

    public ListViewModel() {
        Timber.i("users fetched");
        fetchUsers();
    }

    //immutable as oppose to MutableLiveData
    //The view only gets and NOT sets the data
    LiveData<List<User>> getUsers(){
        return users;
    }

    LiveData<Boolean> getError(){
        return userLoadError;
    }

    LiveData<Boolean> getLoading() {
        return loading;
    }

    //this will get our users with networking
    private void fetchUsers() {
        loading.setValue(true);
        //huge line that is built using retrofit api class and Api service
        userCall = UserApi.getInstance().getApiUserList();
        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                //set your error live data to false
                userLoadError.setValue(false);
                Timber.i("response.body(): "+response.body().toString());
                users.setValue(response.body());
                loading.setValue(false);
                userCall = null;
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Timber.e(getClass().getSimpleName(), "Error loading users", t);
                userLoadError.setValue(true);
                loading.setValue(false);
                userCall = null;
            }
        });
    }

    @Override
    protected void onCleared() {
        if(userCall != null){
            userCall.cancel();
            userCall = null;
        }

    }
}
