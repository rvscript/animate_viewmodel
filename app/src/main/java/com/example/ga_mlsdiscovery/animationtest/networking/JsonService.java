package com.example.ga_mlsdiscovery.animationtest.networking;

import com.example.ga_mlsdiscovery.animationtest.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonService {
@GET("users")
    Call<List<User>> getApiUserList();
}
