package com.example.ga_mlsdiscovery.animationtest.networking;

import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class UserApi {
    //Our base url
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/";

    //JsonService Api service class
    private static JsonService jsonService;

    //Retrofit instance
    private static Retrofit retrofit;

    //initialize our static Retrofit
    private static void initializeRetrofit(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
    }

    //JsonService instance method
    public static JsonService getInstance(){
        if(jsonService != null){
            return jsonService;
        }
        if(retrofit == null){
            initializeRetrofit();
        }
        //once we have  a jsonService and a retrofit instance we can return
        //our json service instance
        jsonService = retrofit.create(JsonService.class);
        return jsonService;
    }

    private UserApi(){
        //private constructor to prevent anyone from creating an additional instance
    }
}
