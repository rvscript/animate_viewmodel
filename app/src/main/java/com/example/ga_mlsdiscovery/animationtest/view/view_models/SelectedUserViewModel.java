package com.example.ga_mlsdiscovery.animationtest.view.view_models;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.ga_mlsdiscovery.animationtest.model.User;

public class SelectedUserViewModel extends ViewModel{
    private final MutableLiveData<User> selectedUser = new MutableLiveData<>();
    //getter for selectedUser
    public LiveData<User> getSelectedUser(){
        return selectedUser;
    }

    public void setSelectedUser(User user){
        selectedUser.setValue(user);
    }
}
