package com.example.ga_mlsdiscovery.animationtest.view.details_fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ga_mlsdiscovery.animationtest.R;
import com.example.ga_mlsdiscovery.animationtest.view.view_models.SelectedUserViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class UserDetailsFragment extends Fragment {

    private Unbinder unbinder;
    @BindView(R.id.tv_repo_name)TextView userNameTextView;
    @BindView(R.id.tv_repo_description)TextView userDescTextView;
    @BindView(R.id.tv_forks)TextView forksNameTextView;
    @BindView(R.id.tv_stars)TextView starsNameTextView;

    public UserDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_blank, container, false);
        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        dispayRepo();
    }

    private void dispayRepo() {

        //Always call to access view model in any fragment or activity
        //getActivity is the scope. Could be just this. limiting the scope to this fragment
        SelectedUserViewModel selectedUserViewModel = ViewModelProviders.of(getActivity())
                .get(SelectedUserViewModel.class);
        //get ea live data from the view model
        selectedUserViewModel.getSelectedUser().observe(this, user ->{
            userNameTextView.setText(user.getUsername());
            userDescTextView.setText(user.getId()+", UN:"+user.getUsername());
            forksNameTextView.setText(String.valueOf(user.getPhone()));
            starsNameTextView.setText(String.valueOf(user.getEmail()));
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder != null){
            unbinder.unbind();
            unbinder = null;
        }
    }
}
