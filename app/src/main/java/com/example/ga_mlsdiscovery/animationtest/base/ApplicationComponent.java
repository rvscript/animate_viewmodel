package com.example.ga_mlsdiscovery.animationtest.base;

import com.example.ga_mlsdiscovery.animationtest.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/*
Super important class
Add Modules to this component annotation interface
 */


//When using this component you can use the prescribed modules
@Singleton
@Component(modules = {
        NetworkModule.class
})
public interface ApplicationComponent {
}
