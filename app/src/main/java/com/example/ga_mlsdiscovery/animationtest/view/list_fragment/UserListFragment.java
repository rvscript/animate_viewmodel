package com.example.ga_mlsdiscovery.animationtest.view.list_fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ga_mlsdiscovery.animationtest.R;
import com.example.ga_mlsdiscovery.animationtest.model.User;
import com.example.ga_mlsdiscovery.animationtest.view.details_fragment.UserDetailsFragment;
import com.example.ga_mlsdiscovery.animationtest.view.interfaces.UserSelectedListener;
import com.example.ga_mlsdiscovery.animationtest.view.view_models.SelectedUserViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class UserListFragment extends Fragment implements UserSelectedListener{
    @BindView(R.id.text_error)TextView textError;
    @BindView(R.id.recyclerview)RecyclerView recyclerView;
    @BindView(R.id.loading_view)View progressBar;

    private Unbinder unbinder;

    private ListViewModel viewModel;

    public UserListFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //The Line that makes view model items accessible
        viewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        observeViewModel();
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        //UserListAdapter(ViewModel, LifecycleOwner, UserSelectedListener//for onClick)
        recyclerView.setAdapter(new UserListAdapter(viewModel, this, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void observeViewModel() {
        //this is how we subscribe to the list from viewModel
        viewModel.getUsers().observe(this, users ->{
            if(users != null){
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        viewModel.getError().observe(this, isError ->{
            if(isError){
                textError.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                textError.setText("error loading users");
            } else {
                textError.setVisibility(View.GONE);
                textError.setText(null);
            }
        });

        viewModel.getLoading().observe(this, isLoading ->{
            progressBar.setVisibility(isLoading?View.VISIBLE:View.GONE);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder != null){
            unbinder.unbind();
            unbinder = null;
        }
    }
    /*
        ViewModelProviders.of(getActivity()).get(SelectedRepoViewModel.class) is how we can access a
        viewModel with multiple fragments.
        The scope is getActivity()
         */
    @Override
    public void onUserSelected(User user) {
        //scoped to the activity for use by both fragments
        SelectedUserViewModel selectedUserViewModel = ViewModelProviders.of(getActivity())
                .get(SelectedUserViewModel.class);
        selectedUserViewModel.setSelectedUser(user);

        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new UserDetailsFragment())
                .addToBackStack(null)
                .commit();
    }
}
