package com.example.ga_mlsdiscovery.animationtest.networking;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class NetworkModule {
    //Dagger NetworkModule
    private static final String BASE_URL = "https://api.github.com/";

    @Provides
    @Singleton
    static Retrofit provideRetrofit(){
        //all methods used in dagger need an @Provides
        //this one is for Retrofit Api builder
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
    }
}
